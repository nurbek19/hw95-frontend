import React from 'react';
import {PageHeader} from "react-bootstrap";

const MainPage = () => (
    <PageHeader style={{padding: '200px 0'}}>Для того чтобы начать пользоваться, пожалуйста залогиньтесь!</PageHeader>
);

export default MainPage;