import React, {Component, Fragment} from 'react';
import {Alert, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import {connect} from "react-redux";
import {loginUser} from "../../store/actions/users";
import FacebookLogin from "../../components/Auth/FacebookLogin/FacebookLogin";

class Login extends Component {

    submitFormHandler = event => {
        event.preventDefault();
    };

    render() {
        return (
            <Fragment>
                <PageHeader>Login</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>
                    {this.props.error &&
                        <Alert bsStyle="danger">{this.props.error.error}</Alert>
                    }

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <FacebookLogin />
                        </Col>
                    </FormGroup>

                </Form>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    error: state.users.loginError
});

const mapDispatchToProps = dispatch => ({
    loginUser: userData => dispatch(loginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);