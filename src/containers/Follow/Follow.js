import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup, PageHeader} from "react-bootstrap";
import {followUser} from "../../store/actions/users";

class Follow extends Component {
    state = {
      email: ''
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.followUser(this.state);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    render() {
        return(
            <Fragment>
                <PageHeader>Follow</PageHeader>

                <Form horizontal onSubmit={this.submitFormHandler}>
                    <FormGroup controlId="formHorizontalEmail">
                        <Col componentClass={ControlLabel} sm={2}>
                            Email
                        </Col>
                        <Col sm={10}>
                            <FormControl
                                type="email"
                                placeholder="Email"
                                name="email"
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button type="submit">Find</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
   successMessage: state.users.followMessage
});

const mapDispatchToProps = dispatch => ({
    followUser: email => dispatch(followUser(email))
});

export default connect(mapStateToProps, mapDispatchToProps)(Follow);