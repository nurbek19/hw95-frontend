import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader, Panel} from "react-bootstrap";
import {fetchFollowers, removeFollower} from "../../store/actions/users";

class Followers extends Component {
    componentDidMount() {
        this.props.fetchFollowers();
    }

    render() {
        return (
            <Fragment>
                <PageHeader>Followers</PageHeader>

                {this.props.friends.map(friend => (
                    <Panel key={friend._id}>
                        <Panel.Body>
                            <h4>{friend.username}</h4>
                            <Button bsStyle="danger" onClick={() => this.props.removeFollower(friend._id)}>Delete</Button>
                        </Panel.Body>
                    </Panel>
                ))}
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    friends: state.users.friends
});

const mapDispatchToProps = dispatch => ({
    fetchFollowers: () => dispatch(fetchFollowers()),
    removeFollower: id => dispatch(removeFollower(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Followers);