import React, {Component, Fragment} from 'react';
import { Col, Form, FormGroup, PageHeader} from "react-bootstrap";

import {registerUser} from "../../store/actions/users";
import {connect} from "react-redux";
import FacebookLogin from "../../components/Auth/FacebookLogin/FacebookLogin";

class Register extends Component {

    submitFormHandler = event => {
        event.preventDefault();
    };

    render() {
        return (
            <Fragment>
                <PageHeader>Register new user</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <FacebookLogin />
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);