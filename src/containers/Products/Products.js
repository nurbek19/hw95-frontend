import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {deleteProduct, fetchProducts} from "../../store/actions/products";

import ProductListItem from '../../components/ProductListItem/ProductListItem';

class Products extends Component {
    componentDidMount() {
        this.props.onFetchProducts();
    }



    render() {
        return (
            <Fragment>
                <h2>Events</h2>

                {this.props.products.map(product => (
                    <ProductListItem
                        key={product._id}
                        id={product._id}
                        date={product.date}
                        title={product.title}
                        duration={product.duration}
                        productUser={product.user._id}
                        user={this.props.user}
                        remove={() => this.props.deleteProduct(product._id)}
                    />
                ))}
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        products: state.products.products,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchProducts: () => dispatch(fetchProducts()),
        deleteProduct: id => dispatch(deleteProduct(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);