import React from 'react';
import {Route, Switch} from "react-router-dom";

import NewProduct from "./containers/NewProduct/NewProduct";
import Products from "./containers/Products/Products";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import Follow from "./containers/Follow/Follow";
import Followers from "./containers/Followers/Followers";
import MainPage from "./containers/MainPage/MainPage";

const Routes = () => {
    return (
        <Switch>
            <Route path="/" exact component={MainPage}/>
            <Route path="/events" exact component={Products}/>
            <Route
                path="/products/new"
                exact
                component={NewProduct}
            />
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/follow" exact component={Follow}/>
            <Route path="/followers" exact component={Followers}/>
        </Switch>
    );
};

export default Routes;