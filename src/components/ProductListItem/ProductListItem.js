import React from 'react';
import {Button, Panel} from "react-bootstrap";
import PropTypes from 'prop-types';


const ProductListItem = props => {

    let show = false;

    if (props.user && (props.user._id === props.productUser)) {
        show = true;
    }

    return (
        <Panel key={props.id}>
            <Panel.Body>
                <h4>{props.date}</h4>
                <p>{props.title}</p>
                <strong>
                    {props.duration}
                </strong>
                {show && <Button bsStyle="danger" onClick={props.remove}>Delete</Button>}
            </Panel.Body>
        </Panel>
    )
};

ProductListItem.propTypes = {
    id: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    duration: PropTypes.string.isRequired,
};

export default ProductListItem;