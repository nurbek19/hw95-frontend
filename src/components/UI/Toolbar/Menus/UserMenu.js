import React, {Fragment} from 'react';
import {Nav, NavDropdown} from 'react-bootstrap';
import {LinkContainer} from "react-router-bootstrap";
import MenuItem from "react-bootstrap/es/MenuItem";

const UserMenu = ({user, logout}) => {
    const navTitle = (
        <Fragment>
            Hello, <b>{user.username}</b>
        </Fragment>
    );

    return (
        <Nav pullRight>
            <NavDropdown title={navTitle} id="user-menu">
                <LinkContainer to="/events" exact>
                    <MenuItem>My events</MenuItem>
                </LinkContainer>
                <MenuItem divider/>
                <LinkContainer to="/products/new" exact>
                    <MenuItem>Add event</MenuItem>
                </LinkContainer>
                <MenuItem divider/>
                <LinkContainer to="/follow" exact>
                    <MenuItem>Follow</MenuItem>
                </LinkContainer>
                <MenuItem divider/>
                <LinkContainer to="/followers" exact>
                    <MenuItem>Followers</MenuItem>
                </LinkContainer>
                <MenuItem divider/>
                <MenuItem onClick={logout}>Logout</MenuItem>
            </NavDropdown>

        </Nav>
    )
};

export default UserMenu;