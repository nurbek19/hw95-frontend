import React from 'react';
import {Navbar} from "react-bootstrap";
import UserMenu from "./Menus/UserMenu";
import AnonymousMenu from "./Menus/AnonymousMenu";

const Toolbar = ({user, logout}) => {
    return (
        <Navbar>
            <Navbar.Header>
                <Navbar.Brand>
                    Events
                    {/*<LinkContainer to="/events" exact><a></a></LinkContainer>*/}
                </Navbar.Brand>
                <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
                {user ? <UserMenu user={user} logout={logout} /> : <AnonymousMenu />}
            </Navbar.Collapse>
        </Navbar>

    )
};

export default Toolbar;