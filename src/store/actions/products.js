import {
    CREATE_PRODUCT,
    CREATE_PRODUCT_SUCCESS,
    DELETE_PRODUCT,
    DELETE_PRODUCT_SUCCESS,
    FETCH_PRODUCT,
    FETCH_PRODUCT_SUCCESS
} from "./actionTypes";


export const fetchProductSuccess = products => {
    return {type: FETCH_PRODUCT_SUCCESS, products}
};

export const fetchProducts = () => {
    return {type: FETCH_PRODUCT}
};

export const createProductSuccess = () => {
  return {type: CREATE_PRODUCT_SUCCESS}
};

export const createProduct = productData => {
    return {type: CREATE_PRODUCT, productData};
};

export const deleteProductSuccess = id => ({ type: DELETE_PRODUCT_SUCCESS, id });

export const deleteProduct = id => {
    return {type: DELETE_PRODUCT, id}
};
