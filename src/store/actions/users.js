import {push} from 'react-router-redux';
import axios from '../../axios-api';
import {NotificationManager} from 'react-notifications';
import {
    FETCH_FOLLOWERS, FETCH_FOLLOWERS_SUCCESS,
    FOLLOW_USER, FOLLOW_USER_FAILURE, FOLLOW_USER_SUCCESS,
    LOGIN_USER_SUCCESS,
    LOGOUT_USER,
    REGISTER_USER,
    REGISTER_USER_FAILURE,
    REGISTER_USER_SUCCESS, REMOVE_FOLLOWER, REMOVE_FOLLOWER_SUCCESS
} from "./actionTypes";

export const registerUserSuccess = () => {
    return {type: REGISTER_USER_SUCCESS};
};

export const registerUserFailure = error => {
    return {type: REGISTER_USER_FAILURE, error}
};

export const registerUser = userData => {
    return {type: REGISTER_USER, userData}
};

export const followUserSuccess = () => {
  return {type: FOLLOW_USER_SUCCESS};
};

export const followUserFailure = () => {
    return {type: FOLLOW_USER_FAILURE}
};

export const fetchFollowersSuccess = friends => {
   return {type: FETCH_FOLLOWERS_SUCCESS, friends};
};

export const removeFollowerSuccess = id => ({
   type: REMOVE_FOLLOWER_SUCCESS, id
});


export const followUser = email => {
  return {type: FOLLOW_USER, email}
};

export const fetchFollowers = () => {
  return {type: FETCH_FOLLOWERS}
};

export const removeFollower = id => {
  return {type: REMOVE_FOLLOWER, id}
};

const loginUserSuccess = (user, token) => {
    return {type: LOGIN_USER_SUCCESS, user, token};
};

const loginUserFailure = error => {
    return {type: REGISTER_USER_FAILURE, error};
};

export const loginUser = userData => {
    return dispatch => {
        return axios.post('/users/sessions', userData).then(
            response => {
                dispatch(loginUserSuccess(response.data.user, response.data.token));
                dispatch(push('/events'));
                NotificationManager.success('Success', 'Login successful');
            },
            error => {
                const errorObj = error.response ? error.response.data : {error: 'No internet connection'};
                dispatch(loginUserFailure(errorObj));
            }
        )
    }
};

export const logoutUser = () => {
  return (dispatch, getState) => {
      const token = getState().users.user.token;
      const headers = {'Token': token};

      return axios.delete('/users/sessions', {headers}).then(
          response => {
              dispatch({type: LOGOUT_USER});
              dispatch(push('/'));
              NotificationManager.success('Success', 'Logout successful');
          },
          error => {
              NotificationManager.error('Error', 'Could not logout');
          }
      )
  }
};


export const logoutExpiredUser = () => {
  return dispatch => {
      dispatch({type: LOGOUT_USER});
      dispatch(push('/login'));
      NotificationManager.error('Error', 'Your session has expired, please login again');

  }
};

export const facebookLogin = data => {
    return dispatch => {
        axios.post('/users/facebookLogin', data).then(
            response => {
                dispatch(loginUserSuccess(response.data.user, response.data.token));
                dispatch(push('/events'));
                NotificationManager.success('Logged in with Facebook!', 'Success');
            },
            error => {
                dispatch(loginUserFailure(error.response.data));
            }
        )
    };
};

