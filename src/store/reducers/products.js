import {DELETE_PRODUCT_SUCCESS, FETCH_PRODUCT_SUCCESS} from "../actions/actionTypes";

const initialState = {
    products: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PRODUCT_SUCCESS:
            return {...state, products: action.products};
        case DELETE_PRODUCT_SUCCESS:
            const products = [...state.products];

            const index = products.findIndex(
                product => product._id === action.id,
            );

            products.splice(index, 1);

            return { ...state, products};
        default:
            return state;
    }
};

export default reducer;