import {
    FETCH_FOLLOWERS_SUCCESS,
    LOGIN_USER_FAILURE,
    LOGIN_USER_SUCCESS, LOGOUT_USER,
    REGISTER_USER_FAILURE,
    REGISTER_USER_SUCCESS, REMOVE_FOLLOWER_SUCCESS
} from "../actions/actionTypes";

const initialState = {
    registerError: null,
    loginError: null,
    user: null,
    token: null,
    friends: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case REGISTER_USER_SUCCESS:
            return {...state, registerError: null};
        case REGISTER_USER_FAILURE:
            return {...state, registerError: action.error};
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                user: action.user,
                token: action.token,
                loginError: null
            };
        case LOGIN_USER_FAILURE:
            return {...state, loginError: action.error};
        case LOGOUT_USER:
            return {...state, user: null};
        case FETCH_FOLLOWERS_SUCCESS:
            return {...state, friends: action.friends};
        case REMOVE_FOLLOWER_SUCCESS:
            const friends = [...state.friends];

            const index = friends.findIndex(
                friend => friend.id === action.id,
            );

            friends.splice(index, 1);

            return { ...state, friends};
        default:
            return state;
    }
};

export default reducer;