import {takeEvery, all} from 'redux-saga/effects';
import {
    CREATE_PRODUCT,
    DELETE_PRODUCT,
    FETCH_FOLLOWERS,
    FETCH_PRODUCT,
    FOLLOW_USER,
    REGISTER_USER,
    REMOVE_FOLLOWER
} from "../actions/actionTypes";
import {fetchFollowersSaga, followUserSaga, registerUserSaga, removeFollowerSaga} from "./users";
import {createProductSaga, deleteProductSaga, fetchProductsSaga} from "./products";


export function* watchRegisterUser() {
    yield takeEvery(REGISTER_USER, registerUserSaga);
}

export function* whatchFetchProducts() {
    yield takeEvery(FETCH_PRODUCT, fetchProductsSaga)
}

export function* whatchDeleteProduct() {
    yield takeEvery(DELETE_PRODUCT, deleteProductSaga)
}

export function* whatchFollowUser() {
    yield takeEvery(FOLLOW_USER, followUserSaga)
}

export function* watchFetchFollowers() {
    yield takeEvery(FETCH_FOLLOWERS, fetchFollowersSaga)
}

export function* whatchRemoveFollower() {
    yield takeEvery(REMOVE_FOLLOWER, removeFollowerSaga)
}

export function* watchCreateProduct() {
    yield takeEvery(CREATE_PRODUCT, createProductSaga)
}

export default function* rootSaga() {
    yield all([watchRegisterUser(), whatchFetchProducts(),
        whatchDeleteProduct(), whatchFollowUser(),
        watchFetchFollowers(), whatchRemoveFollower(),
        watchCreateProduct()
    ])
}