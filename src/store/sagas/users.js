import axios from "../../axios-api";
import {put} from 'redux-saga/effects'
import {push} from "react-router-redux";
import {NotificationManager} from "react-notifications";
import {
    fetchFollowersSuccess,
    followUserFailure,
    followUserSuccess,
    registerUserFailure,
    registerUserSuccess,
    removeFollowerSuccess
} from "../actions/users";

export function* registerUserSaga(action) {
    try {
        yield axios.post('/users', action.userData);
        yield put(registerUserSuccess());
        yield put(push('/'));
        yield NotificationManager.success('Success', 'Registration successful');
    } catch (e) {
        yield put(registerUserFailure(e.response.data))
    }
}

export function* followUserSaga(action) {
    try {
        yield axios.put('/users/follow', action.email);
        yield put(followUserSuccess());
        yield put(push('/events'));
        yield NotificationManager.success('Success', 'You successfully followed this user');
    } catch (e) {
        yield put(followUserFailure());
        yield NotificationManager.error('Error', 'User not found');
    }
}

export function* fetchFollowersSaga() {
    const response = yield axios.get('/users/followers');
    yield put(fetchFollowersSuccess(response.data));
}

export function* removeFollowerSaga(action) {
    yield axios.delete(`/users/followers/${action.id}`);
    yield put(removeFollowerSuccess(action.id))
}