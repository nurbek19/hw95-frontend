import axios from "../../axios-api";
import {put} from 'redux-saga/effects'
import {createProductSuccess, deleteProductSuccess, fetchProductSuccess} from "../actions/products";
import {push} from "react-router-redux";

export function* fetchProductsSaga() {
    const response = yield axios.get('/products');
    yield put(fetchProductSuccess(response.data));
}

export function* deleteProductSaga(action) {
    yield axios.delete(`/products/${action.id}`);
    yield put(deleteProductSuccess(action.id));
}

export function* createProductSaga(action) {
    axios.post('/products', action.productData);
    yield put(createProductSuccess());
    yield put(push('/events'));
}