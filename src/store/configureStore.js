import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {routerMiddleware, routerReducer} from "react-router-redux";
import thunkMiddleware from "redux-thunk";
import createHistory from "history/createBrowserHistory";
import createSagaMiddleware from 'redux-saga';

import {saveState, loadState} from "./localStorage";
import productsReducer from "./reducers/products";
import usersReducer from "./reducers/users";
import rootSaga from './sagas';

const rootReducer = combineReducers({
    products: productsReducer,
    users: usersReducer,
    routing: routerReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const history = createHistory();

const sagaMiddleware = createSagaMiddleware();

const middleware = [
    thunkMiddleware,
    routerMiddleware(history),
    sagaMiddleware
];

const enhancers = composeEnhancers(applyMiddleware(...middleware));



const persistedState = loadState();

const store = createStore(rootReducer, persistedState, enhancers);

sagaMiddleware.run(rootSaga);

store.subscribe(() => {
    saveState({
        users: store.getState().users
    })
});

export default store;
